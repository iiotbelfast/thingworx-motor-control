package main;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 *
 *
 * Description:     This application controls a Raspberry Pi 3b connected to a small conveyor-belt motor.
 *                  Sensors are placed on the motor and its power supply. Measuring voltage, current,
 *                  ambient temperature, chassis temperatureSensor, and chassis vibration.
 */


// Import Declarations
import main.comms.thingworx.ThingWorxController;
import main.system.arduino.ArduinoNanoController;
import main.system.raspberrypi.RPI3bController;
import com.thingworx.types.InfoTable;


public class SystemController {

    // System Object Declarations
    // --------------------------
    private static main.Motor Motor;
    private static ThingWorxController ThingWorxController;


    public static void main(String[] args) {

        // Initialise RPI3b Controller as Singleton
        RPI3bController.rpi3b.constructor();

        // Initialise Arduino Controller as Singleton
        ArduinoNanoController.arduino.constructor();


        // Initialise - ThingWorx Cloud Service
        ThingWorxController = new ThingWorxController(
                "wss://iiot.deloitteindustrialdata.co.uk:443/Thingworx/WS",
                "df922fa9-a7e4-44bf-86ff-0eb91498e706",
                "AMRC_Motor-Control_CBM001",
                "AMRC_CBM001",
                "AMRC Conveyor belt motor #001",
                1000);

        Thread ThingWorxControllerThread = new Thread(ThingWorxController);
        ThingWorxControllerThread.start();


        // Initialise new main.Motor object
        System.out.println("### ===================================================================");
        System.out.println("###");
        System.out.println("###   SYSTEM      - Creating main.Motor object");
        Motor = new Motor("CBM001","Conveyor belt motor 001", 1000);
        // Create main.Motor Thread
        System.out.println("###   SYSTEM      - Creating main.Motor thread");
        Thread MotorThread = new Thread(Motor);
        //Thread motorThread = new Thread(main.Motor);
        System.out.println("###   SYSTEM      - Starting main.Motor thread");
        System.out.println("###");
        System.out.println("### ===================================================================");
        MotorThread.start();
    }

//======================================================================================================================


    public synchronized static void powerOn (){
        Motor.powerOn();
    }

    public synchronized static void powerOff(){
        Motor.powerOff();
    }

    public synchronized static void setPower(boolean state){
        if (state == true) {
            Motor.powerOn();
        } else {
            Motor.powerOff();
        }
    }

    public synchronized static void sendValues (float chassisTemperature ,float ambientTemperature, float voltage, float current, float vibration, InfoTable spectralAnalysis, boolean powerStatus){
        ThingWorxController.sendValues(chassisTemperature ,ambientTemperature, voltage, current, vibration, spectralAnalysis, powerStatus);
        System.out.println("###      SYSTEM     - Passing sensor values to ThingWorx connection");
    }
}


