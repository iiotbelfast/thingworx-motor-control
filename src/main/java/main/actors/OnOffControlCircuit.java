package main.actors;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import main.system.raspberrypi.RPI3bController;


public class OnOffControlCircuit {

    // Property Declarations
    private String circuitName = "";
    private int gpioCtrlPin = 99;


    // Object Constructor
    public OnOffControlCircuit(String circuitName, int gpioCtrlPin) {
        this.circuitName = circuitName;
        this.gpioCtrlPin = gpioCtrlPin;
    }


//======================================================================================================================


    public boolean getCircuitStatus() {
        return RPI3bController.rpi3b.getPinState(gpioCtrlPin);
    }


//----------------------------------------------------------------------------------------------------------------------


    public void setCircuitStatus(boolean circuitStatus) {
        if (circuitStatus == true) {
            RPI3bController.rpi3b.switchOn(this.gpioCtrlPin);
        } else {
            RPI3bController.rpi3b.switchOff(this.gpioCtrlPin);
        }
    }

    public void switchOn() {
        //System.out.println("###");
        //System.out.println("### -         I am " + this.circuitName + " and I am switching on with pin " + this.gpioCtrlPin);
        RPI3bController.rpi3b.switchOn(this.gpioCtrlPin);
    }

    public void switchOff() {
        //System.out.println("###");
        //System.out.println("### -         I am " + this.circuitName + " and I am switching off with pin " + this.gpioCtrlPin);
        RPI3bController.rpi3b.switchOff(this.gpioCtrlPin);
    }
}