package main.comms.thingworx;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import com.thingworx.communications.client.ClientConfigurator;
import com.thingworx.communications.client.ConnectedThingClient;


public class MotorThingClient extends ConnectedThingClient {


    public MotorThingClient(ClientConfigurator config) throws Exception {
        super(config);
    }
}