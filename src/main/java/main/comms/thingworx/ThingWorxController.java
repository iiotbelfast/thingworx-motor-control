package main.comms.thingworx;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import com.thingworx.communications.client.ClientConfigurator;
import com.thingworx.communications.client.things.VirtualThing;
import com.thingworx.communications.common.SecurityClaims;
import com.thingworx.types.InfoTable;


public class ThingWorxController implements Runnable{

    // Property Declarations
    private String uri = "";
    private String appKey = "";
    private String name = "";
    private String identifier = "";
    private String description = "";
    private int pollRate = 1000;
    private MotorThingClient client;
    private MotorThing motorThing;


// =====================================================================================================================


    // Object Constructor
    public ThingWorxController(String uri, String appKey, String name, String identifier, String description, int pollRate) {
        this.uri = uri;
        this.appKey = appKey;
        this.name = name;
        this.identifier = identifier;
        this.description = description;
        this.pollRate = pollRate;
    }


// =====================================================================================================================


    public void run() {
        // Set the required configuration information
        ClientConfigurator config = new ClientConfigurator();
        // The uri for connecting to Thingworx
        config.setUri(this.uri);
        //System.out.println("URI is:" + this.uri);
        // Reconnect every 15 seconds if a disconnect occurs or if initial connection cannot be made
        config.setReconnectInterval(15);

        // Set the security using an Application Key
        SecurityClaims claims = SecurityClaims.fromAppKey(this.appKey);
        config.setSecurityClaims(claims);

        // Set the name of the client
        config.setName(this.name);
        //System.out.println("Name is:" + this.name);

        // This client is a SDK
        config.setAsSDKType();

        // This will allow us to test against a server using a self-signed certificate.
        // This should be removed for production systems.
        config.ignoreSSLErrors(true); // All self signed certs

        // Get the scan rate (milliseconds) that is specific to this example
        // The example will execute the processScanRequest of the VirtualThing
        // based on this scan rate
        int scanRate = this.pollRate;
        System.out.println("Poll Rate is:" + this.pollRate);

        // Create new 'Client' using above config
        try {
            client = new MotorThingClient(config);
        } catch (Exception e) {
            System.out.println("!!! ERROR !!!   - Failed to create client: " + e.getMessage());
        }

        // Create new 'Thing'
        System.out.println("###      THING      - Creating MotorThing");
        motorThing = new MotorThing("AMRC_CBM-001","AMRC Conveyor belt motor #001", "AMRCCBM001" , client);
        try {
            // Bind new 'Thing' to 'Client'
            System.out.println("Binding Motor Thing");
            client.bindThing(motorThing);
        } catch (Exception e) {
            System.out.println("!!! ERROR !!!   - Failed to bind client: " + e.getMessage());
        }

        try {
            // Start the client
            client.start();
        }
        catch(Exception e) {
            System.out.println("!!! ERROR !!!   - Failed to start client: " + e.getMessage());
        }

        // As long as the client has not been shutdown, continue
        while(!client.isShutdown()) {
            // Only process the Virtual Things if the client is connected
            if(client.isConnected()) {
                // Loop over all the Virtual Things and process them
                for(VirtualThing thing : client.getThings().values()) {
                    try {

                        System.out.println("###      THING      - Processing Scan Request");
                        thing.processScanRequest();
                    }
                    catch(Exception eProcessing) {
                        System.out.println("Error Processing Scan Request for [" + thing.getName() + "] : " + eProcessing.getMessage());
                    }
                }
            }
            // Suspend processing at the scan rate interval
            try {
                Thread.sleep(scanRate);
            } catch (Exception e) {
                System.out.println("Initial Start Failed : " + e.getMessage());
            }
        }
    }


// =====================================================================================================================


    public void sendValues (float chassisTemperature ,float ambientTemperature, float voltage, float current, float vibration, InfoTable vibrationInfoTable, boolean powerStatus){
        motorThing.setValues(chassisTemperature,ambientTemperature, voltage, current, vibration, vibrationInfoTable, powerStatus);
    }
}
