package main.comms.thingworx;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import com.thingworx.metadata.PropertyDefinition;
import com.thingworx.types.primitives.IPrimitiveType;
import com.thingworx.communications.client.ConnectedThingClient;
import com.thingworx.communications.client.things.VirtualThing;
import com.thingworx.metadata.FieldDefinition;
import com.thingworx.types.InfoTable;
import com.thingworx.metadata.annotations.ThingworxEventDefinition;
import com.thingworx.metadata.annotations.ThingworxEventDefinitions;
import com.thingworx.metadata.annotations.ThingworxPropertyDefinition;
import com.thingworx.metadata.annotations.ThingworxPropertyDefinitions;
import com.thingworx.metadata.annotations.ThingworxServiceDefinition;
import com.thingworx.metadata.annotations.ThingworxServiceResult;
import com.thingworx.metadata.collections.FieldDefinitionCollection;
import com.thingworx.types.BaseTypes;
import com.thingworx.types.constants.CommonPropertyNames;
import main.SystemController;


//======================================================================================================================


// Remote Property Definitions
@SuppressWarnings("serial")
@ThingworxPropertyDefinitions(properties = {
        @ThingworxPropertyDefinition(
                name="Prop_Temp_Casing",
                description="main.Motor Temperature (Degrees Celsius)",
                baseType="NUMBER",
                category="Status",
                aspects={
                        "dataChangeType:ALWAYS",
                        "dataChangeThreshold:0",
                        "cacheTime:0",
                        "isPersistent:false",
                        "isReadOnly:TRUE",
                        "pushType:ALWAYS",
                        "defaultValue:0",
                        "isFolded:false"}),

        @ThingworxPropertyDefinition(
                name="Prop_Temp_Ambient",
                description="Ambient Temperature (Degrees Celsius)",
                baseType="NUMBER",
                category="Status",
                aspects={
                        "dataChangeType:ALWAYS",
                        "dataChangeThreshold:0",
                        "cacheTime:0",
                        "isPersistent:false",
                        "isReadOnly:TRUE",
                        "pushType:ALWAYS",
                        "defaultValue:0",
                        "isFolded:false"}),

        @ThingworxPropertyDefinition(
                name="Prop_Voltage",
                description="Voltage Supplied (Volts)",
                baseType="NUMBER",
                category="Status",
                aspects={
                        "dataChangeType:ALWAYS",
                        "dataChangeThreshold:0",
                        "cacheTime:0",
                        "isPersistent:false",
                        "isReadOnly:TRUE",
                        "pushType:ALWAYS",
                        "defaultValue:0",
                        "isFolded:false"}),

        @ThingworxPropertyDefinition(
                name="Prop_Current",
                description="Current Used (AMPs) ",
                baseType="NUMBER",
                category="Status",
                aspects={
                        "dataChangeType:ALWAYS",
                        "dataChangeThreshold:0",
                        "cacheTime:0",
                        "isPersistent:false",
                        "isReadOnly:TRUE",
                        "pushType:ALWAYS",
                        "defaultValue:0",
                        "isFolded:false"}),

        @ThingworxPropertyDefinition(
                name="Prop_ON_OFF",
                description="main.Motor Power Status (ON / OFF)",
                baseType="BOOLEAN",
                category="Status",
                aspects={
                        "dataChangeType:ALWAYS",
                        "dataChangeThreshold:0",
                        "cacheTime:0",
                        "isPersistent:false",
                        "isReadOnly:TRUE",
                        "pushType:ALWAYS",
                        "defaultValue:0",
                        "isFolded:false"}),

        @ThingworxPropertyDefinition(
                name="Prop_Vibration",
                description="main.Motor Vibration (Vibration)",
                baseType="NUMBER",
                category="Status",
                aspects={
                        "dataChangeType:ALWAYS",
                        "dataChangeThreshold:0",
                        "cacheTime:0",
                        "isPersistent:false",
                        "isReadOnly:TRUE",
                        "pushType:ALWAYS",
                        "defaultValue:0",
                        "isFolded:false"}),

        @ThingworxPropertyDefinition(
                name="spectralAnalysis",
                description="FFT of vibration",
                baseType="INFOTABLE",
                aspects = {
                        "dataChangeType:ALWAYS",
                        "dataChangeThreshold:0",
                        "cacheTime:-1",
                        "isPersistent:FALSE",
                        "isReadOnly:FALSE",
                        "pushType:ALWAYS",
                        "dataShape:spectralAnalysis"
                }
        )
})


//----------------------------------------------------------------------------------------------------------------------


// Remote Event Definitions
@ThingworxEventDefinitions(events = {
        @ThingworxEventDefinition(
                name="Evnt_MotorSensorFault",
                description="main.Motor sensor fault",
                dataShape="MotorSensor.Fault",
                category="Faults",
                isInvocable=true,
                isPropertyEvent=false)
})


//======================================================================================================================


public class MotorThing extends VirtualThing implements Runnable {

    // Property Declatations
    private Thread _shutdownThread = null;

    private static float Prop_Temp_Casing, Prop_Temp_Ambient, Prop_Voltage, Prop_Current, Prop_Vibration;
    private static boolean Prop_ON_OFF;
    private static InfoTable spectralAnalysis;

    private final static String TEMPERATURE_FIELD = "Prop_Temp_Casing";
    private final static String AMBIENT_FIELD = "Prop_Temp_Ambient";
    private final static String VOLTAGE_FIELD = "Prop_Voltage";
    private final static String CURRENT_FIELD = "Prop_Current";
    private final static String VIBRATION_FIELD = "Prop_Vibration";
    private final static String SPECTRALANALYSIS_FIELD = "spectralAnalysis";
    private final static String POWER_STATUS_FIELD = "Prop_ON_OFF";


//======================================================================================================================


    public MotorThing(String name, String description, String identifier, ConnectedThingClient client) {
        super(name, description, identifier, client);

        // Data Shape definition that is used by the motor fault event
        // The event only has one field, the message
        FieldDefinitionCollection faultFields = new FieldDefinitionCollection();
        faultFields.addFieldDefinition(new FieldDefinition(CommonPropertyNames.PROP_MESSAGE, BaseTypes.STRING));
        defineDataShapeDefinition("Evnt_MotorSensor.Fault", faultFields);

        // Populate the thing shape with the properties, services, and events that are annotated in this code
        super.initializeFromAnnotations();
        this.init();
    }


//======================================================================================================================


    public void synchronizeState() {
        // Be sure to call the base class
        super.synchronizeState();
        // Send the property values to Thingworx when a synchronization is required
        super.syncProperties();
    }


    private void init() {
        this.Prop_Temp_Casing = 0.0f;
        this.Prop_Temp_Ambient = 0.0f;
        this.Prop_Voltage = 0.0f;
        this.Prop_Current = 0.0f;
        this.Prop_Vibration = 0.0f;
        this.Prop_ON_OFF = false;

        initializeFromAnnotations();

        FieldDefinitionCollection fields = new FieldDefinitionCollection();
        fields.addFieldDefinition(new FieldDefinition(TEMPERATURE_FIELD, BaseTypes.NUMBER));
        fields.addFieldDefinition(new FieldDefinition(AMBIENT_FIELD, BaseTypes.NUMBER));
        fields.addFieldDefinition(new FieldDefinition(VOLTAGE_FIELD, BaseTypes.NUMBER));
        fields.addFieldDefinition(new FieldDefinition(CURRENT_FIELD, BaseTypes.NUMBER));
        fields.addFieldDefinition(new FieldDefinition(POWER_STATUS_FIELD, BaseTypes.BOOLEAN));
        fields.addFieldDefinition(new FieldDefinition(VIBRATION_FIELD, BaseTypes.NUMBER));
        fields.addFieldDefinition(new FieldDefinition(SPECTRALANALYSIS_FIELD, BaseTypes.INFOTABLE));
    }


    @Override
    public void processScanRequest() throws Exception {
        // Be sure to call the base classes scan request
        super.processScanRequest();
        // Execute the code for this simulation every scan
        System.out.println("###      THING      - Pushing values to ThingWorx cloud");
        this.pushValues();
    }


    public void setValues(float chassisTemperature ,float ambientTemperature, float voltage, float current, float vibration, InfoTable spectralAnalysis, boolean powerStatus) {
        this.Prop_Temp_Casing = chassisTemperature;
        this.Prop_Temp_Ambient = ambientTemperature;
        this.Prop_Voltage = voltage;
        this.Prop_Current = current;
        this.Prop_Vibration = vibration;
        this.Prop_ON_OFF = powerStatus;
        this.spectralAnalysis = spectralAnalysis;
    }


    private void pushValues() throws Exception {
        super.setProperty("Prop_Temp_Casing", this.Prop_Temp_Casing);
        super.setProperty("Prop_Temp_Ambient", this.Prop_Temp_Ambient);
        super.setProperty("Prop_Voltage", this.Prop_Voltage);
        super.setProperty("Prop_Current", this.Prop_Current);
        super.setProperty("Prop_Vibration", this.Prop_Vibration);
        super.setProperty("Prop_ON_OFF", this.Prop_ON_OFF);
        super.setProperty("spectralAnalysis", this.spectralAnalysis);


        System.out.println("==========================================================================");
        System.out.println("==========================================================================");
        System.out.println("###      THING      - Temp_Casing: " + this.Prop_Temp_Casing);
        System.out.println("###      THING      - Temp_Ambient: " + this.Prop_Temp_Ambient);
        System.out.println("###      THING      - Voltage: " + this.Prop_Voltage);
        System.out.println("###      THING      - Current: " + this.Prop_Current);
        System.out.println("###      THING      - Vibration: " +  this.Prop_Vibration);
        System.out.println("###      THING      - Power State: " + this.Prop_ON_OFF);
        System.out.println("==========================================================================");
        System.out.println("==========================================================================");

        // Update the subscribed properties and events to send any updates to Thingworx
        // Without calling these methods, the property and event updates will not be sent
        // The numbers are timeouts in milliseconds.
        super.updateSubscribedProperties(1000);
        super.updateSubscribedEvents(10000);
    }


    @ThingworxServiceDefinition(name = "Shutdown", description = "Shutdown the client")
    @ThingworxServiceResult(name = CommonPropertyNames.PROP_RESULT, description = "", baseType = "NOTHING")
    public synchronized void Shutdown() throws Exception {
        // Should not have to do this, but guard against this method being called more than once.
        if (this._shutdownThread == null) {
            // Create a thread for shutting down and start the thread
            this._shutdownThread = new Thread(this);
            this._shutdownThread.start();
        }
    }


    @Override
    public void run() {
        try {
            // Delay for a period to verify that the Shutdown service will return
            Thread.sleep(1000);
            // Shutdown the client
            this.getClient().shutdown();
        } catch (Exception x) {
            // Not much can be done if there is an exception here
            // In the case of production code should at least log the error
        }
    }


    @Override
    public void processPropertyWrite(PropertyDefinition property, @SuppressWarnings("rawtypes") IPrimitiveType value) throws Exception {
        // Find out which property is being updated
        String propName = property.getName();

        // Remote Power Trigger from ThingWorx platform
        if ("Prop_ON_OFF".equals(propName)) {
            this.setPropertyValue(propName, value);
            SystemController.setPower((boolean) value.getValue());
            //this.Prop_ON_OFF.value = this.get_Prop_ON_OFF();
        } else {
            throw new Exception("The property " + propName + " is read only on the simple device.");
        }

    }
}