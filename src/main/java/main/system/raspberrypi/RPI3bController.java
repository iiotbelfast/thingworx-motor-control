package main.system.raspberrypi;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import com.pi4j.io.gpio.*;


public enum RPI3bController {
    // Declare RPI3bController as Singleton object
    rpi3b;


    // Property Declarations
    private GpioController gpio;
    private GpioPinDigitalOutput GPIO_00;
    /*private GpioPinDigitalOutput GPIO_01;
    private GpioPinDigitalOutput GPIO_02;
    private GpioPinDigitalOutput GPIO_03;
    private GpioPinDigitalOutput GPIO_04;
    private GpioPinDigitalOutput GPIO_05;
    private GpioPinDigitalOutput GPIO_06;
    //private GpioPinDigitalOutput GPIO_07;
    private GpioPinDigitalOutput GPIO_08;
    private GpioPinDigitalOutput GPIO_09;
    private GpioPinDigitalOutput GPIO_10;
    private GpioPinDigitalOutput GPIO_11;
    private GpioPinDigitalOutput GPIO_12;
    private GpioPinDigitalOutput GPIO_13;
    private GpioPinDigitalOutput GPIO_14;
    private GpioPinDigitalOutput GPIO_15;
    private GpioPinDigitalOutput GPIO_16;
    private GpioPinDigitalOutput GPIO_21;
    private GpioPinDigitalOutput GPIO_22;
    private GpioPinDigitalOutput GPIO_23;
    private GpioPinDigitalOutput GPIO_24;
    private GpioPinDigitalOutput GPIO_25;
    private GpioPinDigitalOutput GPIO_26;
    private GpioPinDigitalOutput GPIO_27;
    private GpioPinDigitalOutput GPIO_28;
    private GpioPinDigitalOutput GPIO_29;*/


//======================================================================================================================


    public synchronized void constructor() {
        // Create raspberrypi GPIO controller and pins
        gpio = GpioFactory.getInstance();
        this.cleanUp();

        //System.out.println("###         - Provisioning GPIO Pins");
        GPIO_00 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "Phisical_11-BCM_17-WiringPi_00", PinState.LOW);
        /*GPIO_01 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "Phisical_12-BCM_18-WiringPi_01-PWM0", PinState.LOW);
        GPIO_02 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, "Phisical_13-BCM_27-WiringPi_02", PinState.LOW);
        GPIO_03 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, "Phisical_15-BCM_22-WiringPi_03", PinState.LOW);
        GPIO_04 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "Phisical_16-BCM_23-WiringPi_04", PinState.LOW);
        GPIO_05 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "Phisical_18-BCM_24-WiringPi_05", PinState.LOW);
        GPIO_06 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "Phisical_22-BCM_25-WiringPi_06", PinState.LOW);
        //GPIO_07 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_07, "Phisical_07-BCM_04-WiringPi_07", PinState.LOW);
        // ** Possible Pi4J Driver Issue accessing Pin 7
        GPIO_08 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_08, "Phisical_03-BCM_02-WiringPi_08-SDA", PinState.LOW);
        GPIO_09 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_09, "Phisical_05-BCM_03-WiringPi_09-SCL", PinState.LOW);
        GPIO_10 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_10, "Phisical_24-BCM_08-WiringPi_10-CE0", PinState.LOW);
        GPIO_11 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_11, "Phisical_26-BCM_07-WiringPi_11", PinState.LOW);
        GPIO_12 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_12, "Phisical_19-BCM_10-WiringPi_12-MOSI", PinState.LOW);
        GPIO_13 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_13, "Phisical_21-BCM_09-WiringPi_13-MISO", PinState.LOW);
        GPIO_14 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_14, "Phisical_23-BCM_11-WiringPi_14-SCLK", PinState.LOW);
        GPIO_15 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_15, "Phisical_08-BCM_14-WiringPi_15-TXD", PinState.LOW);
        GPIO_16 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_16, "Phisical_10-BCM_15-WiringPi_16-RXD", PinState.LOW);
        GPIO_21 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_21, "Phisical_29-BCM_05-WiringPi_21", PinState.LOW);
        GPIO_22 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_22, "Phisical_31-BCM_06-WiringPi_22", PinState.LOW);
        GPIO_23 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_23, "Phisical_33-BCM_13-WiringPi_23-PWM1", PinState.LOW);
        GPIO_24 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_24, "Phisical_35-BCM_19-WiringPi_24-MISO", PinState.LOW);
        GPIO_25 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, "Phisical_37-BCM_26-WiringPi_25", PinState.LOW);
        GPIO_26 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26, "Phisical_32-BCM_12-WiringPi_26-PMW0", PinState.LOW);
        GPIO_27 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "Phisical_36-BCM_16-WiringPi_27", PinState.LOW);
        GPIO_28 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_28, "Phisical_38-BCM_20-WiringPi_28", PinState.LOW);
        // ** Used for shutdown button
        GPIO_29 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_29, "Phisical_40-BCM_21-WiringPi_29", PinState.LOW);*/


        //System.out.println("###         - Setting Shutdown States");
        GPIO_00.setShutdownOptions(true, PinState.LOW);
        /*GPIO_01.setShutdownOptions(true, PinState.LOW);
        GPIO_02.setShutdownOptions(true, PinState.LOW);
        GPIO_03.setShutdownOptions(true, PinState.LOW);
        GPIO_04.setShutdownOptions(true, PinState.LOW);
        GPIO_05.setShutdownOptions(true, PinState.LOW);
        GPIO_06.setShutdownOptions(true, PinState.LOW);
        //GPIO_07.setShutdownOptions(true, PinState.LOW);
        GPIO_08.setShutdownOptions(true, PinState.LOW);
        GPIO_09.setShutdownOptions(true, PinState.LOW);
        GPIO_10.setShutdownOptions(true, PinState.LOW);
        GPIO_11.setShutdownOptions(true, PinState.LOW);
        GPIO_12.setShutdownOptions(true, PinState.LOW);
        GPIO_13.setShutdownOptions(true, PinState.LOW);
        GPIO_14.setShutdownOptions(true, PinState.LOW);
        GPIO_15.setShutdownOptions(true, PinState.LOW);
        GPIO_16.setShutdownOptions(true, PinState.LOW);
        GPIO_21.setShutdownOptions(true, PinState.LOW);
        GPIO_22.setShutdownOptions(true, PinState.LOW);
        GPIO_23.setShutdownOptions(true, PinState.LOW);
        GPIO_24.setShutdownOptions(true, PinState.LOW);
        GPIO_25.setShutdownOptions(true, PinState.LOW);
        GPIO_26.setShutdownOptions(true, PinState.LOW);
        GPIO_27.setShutdownOptions(true, PinState.LOW);
        GPIO_28.setShutdownOptions(true, PinState.LOW);
        GPIO_29.setShutdownOptions(true, PinState.LOW);*/
        //System.out.println("### - RPI3b Controller Created");
    }


//======================================================================================================================


    public synchronized boolean getPinState(int gpioControlPin) {
        PinState pinStatus;

        switch (gpioControlPin) {
            case 0:  pinStatus = GPIO_00.getState();
            /*case 1:  pinStatus = GPIO_01.getState();
            case 2:  pinStatus = GPIO_02.getState();
            case 3:  pinStatus = GPIO_03.getState();
            case 4:  pinStatus = GPIO_04.getState();
            case 5:  pinStatus = GPIO_05.getState();
            case 6:  pinStatus = GPIO_06.getState();
            //case 7:  pinStatus = GPIO_07.getState();
            case 8:  pinStatus = GPIO_08.getState();
            case 9:  pinStatus = GPIO_09.getState();
            case 10:  pinStatus = GPIO_10.getState();
            case 11:  pinStatus = GPIO_11.getState();
            case 12:  pinStatus = GPIO_12.getState();
            case 13:  pinStatus = GPIO_13.getState();
            case 14:  pinStatus = GPIO_14.getState();
            case 15:  pinStatus = GPIO_15.getState();
            case 16:  pinStatus = GPIO_16.getState();
            case 21:  pinStatus = GPIO_21.getState();
            case 22:  pinStatus = GPIO_22.getState();
            case 23:  pinStatus = GPIO_23.getState();
            case 24:  pinStatus = GPIO_24.getState();
            case 25:  pinStatus = GPIO_25.getState();
            case 26:  pinStatus = GPIO_26.getState();
            case 27:  pinStatus = GPIO_27.getState();
            case 28:  pinStatus = GPIO_28.getState();
            case 29:  pinStatus = GPIO_29.getState();*/
            default: pinStatus = PinState.LOW;
        }

        if (pinStatus.isHigh()) {
            return true;
        } else
            return false;
    }


    private synchronized void setPinState(int gpioControlPin, PinState pinStatus) {
        switch (gpioControlPin) {
            case 0:  GPIO_00.setState(pinStatus);
            /*case 1:  GPIO_01.setState(pinStatus);
            case 2:  GPIO_02.setState(pinStatus);
            case 3:  GPIO_03.setState(pinStatus);
            case 4:  GPIO_04.setState(pinStatus);
            case 5:  GPIO_05.setState(pinStatus);
            case 6:  GPIO_06.setState(pinStatus);
            //case 7:  GPIO_07.setState(pinStatus);
            case 8:  GPIO_08.setState(pinStatus);
            case 9:  GPIO_09.setState(pinStatus);
            case 10:  GPIO_10.setState(pinStatus);
            case 11:  GPIO_11.setState(pinStatus);
            case 12:  GPIO_12.setState(pinStatus);
            case 13:  GPIO_13.setState(pinStatus);
            case 14:  GPIO_14.setState(pinStatus);
            case 15:  GPIO_15.setState(pinStatus);
            case 16:  GPIO_16.setState(pinStatus);
            case 21:  GPIO_21.setState(pinStatus);
            case 22:  GPIO_22.setState(pinStatus);
            case 23:  GPIO_23.setState(pinStatus);
            case 24:  GPIO_24.setState(pinStatus);
            case 25:  GPIO_25.setState(pinStatus);
            case 26:  GPIO_26.setState(pinStatus);
            case 27:  GPIO_27.setState(pinStatus);
            case 28:  GPIO_28.setState(pinStatus);
            case 29:  GPIO_29.setState(pinStatus);*/
            default: break;
        }
    }


    public synchronized void switchOn(int gpioControlPin) {
        this.setPinState(gpioControlPin, PinState.HIGH);
    }


    public synchronized void switchOff(int gpioControlPin) {
        this.setPinState(gpioControlPin, PinState.LOW);
    }

    public  synchronized void cleanUp() {
        gpio.shutdown();
    }
}
