package main.system.arduino;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import java.io.*;
import java.util.*;
import java.nio.*;


public enum ArduinoNanoController {
    // Declare ArduinoNanoController as Singleton object
    arduino;


    // Property Declarations
    private final static int ARDUINO_ADDRESS = 0x04;
    private static boolean verbose = "true".equals(System.getProperty("i2cDevice.verbose", "false"));
    private I2CBus i2cBus;
    private I2CDevice i2cDevice;


    // ArduinoNanoController Object constructor
    public synchronized void constructor() {
        try {
            i2cBus = I2CFactory.getInstance(I2CBus.BUS_1);
            i2cDevice = i2cBus.getDevice(ARDUINO_ADDRESS);
        } catch (I2CFactory.UnsupportedBusNumberException e) {
            this.closeI2C();
            System.out.println("I2C Exception");
            System.err.println(e.getMessage());
        } catch (IOException e) {
            this.closeI2C();
            System.out.println("IO Exception");
            System.err.println(e.getMessage());
        }
    }


//======================================================================================================================


    public float getValue(int sensorId) {
        int numberOfAnalogSensors = 6;
        byte[] reading = new byte[numberOfAnalogSensors*4];
        float[] values = new float[6];

        try {
            int r = i2cDevice.read(reading, 0, (numberOfAnalogSensors*4)); //Read all six bytes from Arduino array

            for (int i=0; i<numberOfAnalogSensors; i++) {
                try {
                    byte[] valueBytes = Arrays.copyOfRange(reading, (i * 4), (i * 4) + 4);
                    values[i] = ByteBuffer.wrap(valueBytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                    //System.out.println("Array Value:" + values[i]);

                } catch (Exception ex) {
                    this.closeI2C();
                    System.out.println("Failed to populate array");
                    System.err.println(ex.getMessage());
                    ex.printStackTrace();
                }
            }
            //System.out.println("### - Arduino Nano Values Read Successfully");

        } catch (Exception ex) {
            System.out.println("Failed to read array");
            this.closeI2C();
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }

        return values[sensorId];
    }


    public void closeI2C() {
        try {
            this.i2cBus.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
