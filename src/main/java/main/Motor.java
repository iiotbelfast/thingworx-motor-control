package main;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import main.actors.OnOffControlCircuit;
import main.sensors.temperature.temperature_DS18B20;
import main.sensors.current.current_ACS712;
import main.sensors.vibration.vibration_MPU6050;
import main.sensors.voltage.voltage_ZMPT101B;
import com.thingworx.types.InfoTable;


public class Motor implements Runnable {


    // Object Declarations
    private temperature_DS18B20 ambientTemperatureSensor;
    private temperature_DS18B20 chassisTemperatureSensor;
    private current_ACS712 currentSensor;
    private voltage_ZMPT101B voltageSensor;
    private vibration_MPU6050 vibrationSensor;
    private OnOffControlCircuit powerController;
    private InfoTable spectralAnalysis;


    // Property Declarations
    private String id;                              // Motor ID
    private String name;                            // Motor Name
    private int pollRate;                           // Sensor poll & transmit rate
    private float voltage;                          // Average Incoming voltage (volts)
    private float current;                          // Average Current consumed (amps)
    private float chassisTemperature;               // Chassis temperatureSensor (*c)
    private float ambientTemperature;               // Ambient Temperature (*c)
    private float vibration;                        // Operating vibration (g)
    private boolean powerStatus;                    // Power On = True, Power Off = False


//======================================================================================================================


    // MOTOR object constructor
    public Motor(String id, String name, int pollRate) {
        System.out.println("### -------------------------------------------------------------------");
        System.out.println("###      MOTOR      - Initialising main.Motor object");
        // Property Initialisation
        this.id = id;
        this.name = name;
        this.pollRate = pollRate;
        this.voltage = 0.0f;
        this.current = 0.0f;
        this.chassisTemperature = 0.0f;
        this.ambientTemperature = 0.0f;
        this.vibration = 0.0f;
        this.powerStatus = false;



        // Actor Initialisation

            // Initialise:  On/Off Circuit Controller object
                System.out.println("###      MOTOR      - Initialising PowerController object");
                powerController = new OnOffControlCircuit("main.Motor Power Controller", 0);
                powerController.switchOff();

                try {
                    Thread.sleep(5000);
                } catch (Exception e) {

                }


        // Sensor Initialisation

            // Initialise:  Voltage Sensor with RMS Off Value
                System.out.println("###      MOTOR      - Initialising Voltage Sensor");
                voltageSensor = new voltage_ZMPT101B(3,"Voltage Sensor");


            // Initialise:  Current Sensor with RMS Value
                System.out.println("###      MOTOR      - Initialising Current Sensor");
                currentSensor = new current_ACS712(2,"Current Sensor");


            // Initialise:  main.Motor Ambient Temperature Sensor
                System.out.println("###      MOTOR      - Initialising Ambient Temperature Sensor");
                // Belfast
                //ambientTemperatureSensor = new temperature_DS18B20("28-01163012c1ee", "Ambient Temperature");
                // AMRC
                ambientTemperatureSensor = new temperature_DS18B20("28-000009239c06", "Ambient Temperature");


            // Initialise:  main.Motor Chassis Temperature Sensor
                System.out.println("###      MOTOR      - Initialising Chassis Temperature Sensor");
                // Belfast
                //chassisTemperatureSensor = new temperature_DS18B20("28-0000092387c5", "Chassis Temperature");
                // AMRC
                chassisTemperatureSensor = new temperature_DS18B20("28-000009236dd4", "Chassis Temperature");


            // Initialise:  Vibration Sensor with RMS Value
                System.out.println("###      MOTOR      - Initialising Vibration Sensor");
                vibrationSensor = new vibration_MPU6050("0","Vibration Sensor");


        System.out.println("### -------------------------------------------------------------------");
    }


//======================================================================================================================


    public void run() {
        // MOTOR - Main Execution Loop

        while (true) {
            System.out.println("###");
            // GET:  Sensor Data Values from Sensor Objects
            this.ambientTemperature = ambientTemperatureSensor.getValue();
            System.out.println("###      MOTOR      - Ambient Temp: " + this.getAmbientTemperature());
            this.chassisTemperature = chassisTemperatureSensor.getValue();
            System.out.println("###      MOTOR      - Chassis Temp: " + this.getChassisTemperature());
            this.voltage = voltageSensor.getValue();
            System.out.println("###      MOTOR      - Voltage: " + this.getVoltage());
            this.current = currentSensor.getValue();
            System.out.println("###      MOTOR      - Current: " + this.getCurrent());
            this.vibration = vibrationSensor.getValue();
            this.spectralAnalysis = vibrationSensor.getFFT_InfoTable();
            System.out.println("###      MOTOR      - Vibration: " + this.getVibration());


            System.out.println("### -------------------------------------------------------------------");

            // PUSH:  Sensor Data values to PTC - ThingWorx
            System.out.println("###      MOTOR      - Sending readings to ThingWorx");
            SystemController.sendValues(this.chassisTemperature ,this.ambientTemperature, this.voltage, this.current, this.vibration, this.spectralAnalysis, this.powerStatus);
            System.out.println("###");
            System.out.println("### ===================================================================");
            try {
                Thread.sleep(this.pollRate);
            } catch (Exception e) {

            }
        }
    }


//======================================================================================================================


    private float getVoltage() {
        return voltage;
    }

    private float getCurrent() {
        return current;
    }

    private float getChassisTemperature() {
        return chassisTemperature;
    }

    private float getAmbientTemperature() {
        return ambientTemperature;
    }

    private float getVibration() {
        return vibration;
    }

    private boolean getPowerStatus() {
        return powerStatus;
    }


//----------------------------------------------------------------------------------------------------------------------


    private void setVoltage(float voltage) {
        this.voltage = voltage;
    }

    private void setCurrent(float current) {
        this.current = current;
    }

    private void setChassisTemperature(float chassisTemperature) {
        this.chassisTemperature = chassisTemperature;
    }

    private void setAmbientTemperature(float ambientTemperature) {
        this.ambientTemperature = ambientTemperature;
    }

    private void setVibration(float vibration) {
        this.vibration = vibration;
    }


    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


    private void setPowerStatus(boolean powerStatus) {
        this.powerStatus = powerStatus;
    }

    public synchronized void powerOn() {
        setPowerStatus(true);
        powerController.switchOn();
    }

    public synchronized void powerOff() {
        setPowerStatus(false);
        powerController.switchOff();
    }

}