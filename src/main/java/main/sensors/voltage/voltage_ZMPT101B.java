package main.sensors.voltage;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import main.system.arduino.ArduinoNanoController;


public class voltage_ZMPT101B  {


    // Property Declarations
    private int id = 0;
    private String description = "";
    private float value = 0.0f;
    private final float mVperUnit = 1;
    private final float desiredValue = 240.0f;
    private final float highValue = 336.2f;
    private final float noiseLimit = 40.0f;


// =====================================================================================================================


    // Object Constructor
    public voltage_ZMPT101B(int id, String description) {
        this.id = id;
        this.description = description;
        this.setValue(0.0f);
    }


// =====================================================================================================================


    private void setID (int id) {
        this.id = id;
    }

    private void setDescription (String id) {
        this.description = id;
    }

    private void setValue (float sensorValue) {
        this.value = sensorValue;
    }


    // --------------------------------------------------------------------


    public synchronized float getValue () {
        float reading = this.value=ArduinoNanoController.arduino.getValue(id);
        reading = reading / mVperUnit;
        reading = desiredValue*reading/highValue;
        if (reading < noiseLimit) {
            reading = 0.0f;
        }

        return reading;
    }

    private String getDescription () {
        return this.description;
    }

    public synchronized int getID () {
        return this.id;
    }

    // ----------------------------------------------------------------------

}