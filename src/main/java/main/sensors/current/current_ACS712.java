package main.sensors.current;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import main.system.arduino.ArduinoNanoController;


public class current_ACS712 {

    // Property Declarations
    private int id = 0;
    private String description = "";
    private float value = 0.0f;
    private final float mVperUnit = 185;
    private final float desiredValue = 2.2f;
    private final float highValue = 1.464f;
    private final float noiseLimit = 0.4f;


// =====================================================================================================================


    // Object Constructor
    public current_ACS712(int id, String description) {
        this.id = id;
        this.description = description;
        this.setValue(0.0f);
    }


// =====================================================================================================================


    private void setID (int id) {
        this.id = id;
    }

    private void setDescription (String id) {
        this.description = id;
    }

    private void setValue (float sensorValue) {
        this.value = sensorValue;
    }


    // --------------------------------------------------------------------


    public synchronized float getValue () {
        float reading = this.value=ArduinoNanoController.arduino.getValue(id);
        reading = reading / mVperUnit;
        reading = desiredValue*reading/highValue;
        if (reading < noiseLimit) {
            reading = 0.0f;
        }

        return reading;
    }

    private String getDescription () {
        return this.description;
    }

    public synchronized int getID () {
        return this.id;
    }

    // ----------------------------------------------------------------------

}