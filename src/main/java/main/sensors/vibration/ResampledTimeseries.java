package main.sensors.vibration;

/**
 * Created by kpashov on 09/02/2017.
 */

public class ResampledTimeseries {
    double magnitude[];
    double timestamp[];
    int length;

    public  ResampledTimeseries(int fft_size){
        length = fft_size;
        magnitude = new double[length];
        timestamp = new double[length];
    }
}