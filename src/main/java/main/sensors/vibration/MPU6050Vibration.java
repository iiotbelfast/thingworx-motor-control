package main.sensors.vibration;


/**
 * Created by kpashov on 07/02/2017.
 */


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.*;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MPU6050Vibration implements Runnable {
    String filename = "vibrationlog" + new SimpleDateFormat("yyyyMMdd.hh-mm").format(new Date()) + ".log";
    private final String[] args = new String[]{"python","./run-scripts/vibration.py",filename};
    private static final Logger logger = LoggerFactory.getLogger(MPU6050Vibration.class);
    private LinkedList<VibrationVTQ> buffer;

    public MPU6050Vibration(LinkedList<VibrationVTQ> buffer) {
        this.buffer = buffer;
        logger.info("Created new vibration measurement thread successfully.");
    }

    @Override
    public void run() {
        ProcessBuilder pb = new ProcessBuilder(args);
        pb.redirectErrorStream(true);
        while (true) {
            Process p = null;
            try {
                p = pb.start();
                Thread.sleep(100);
                InputStream s;
                while (true) {
                    try {
                        s = new FileInputStream(filename);
                        break;
                    } catch (FileNotFoundException e) {
                        //do nothing
                        logger.error("File not found: " + filename);
                        Thread.sleep(1000);
                    }
                }
                logger.info("Starting new vibration measurement");
                final BufferedReader reader = new BufferedReader(new InputStreamReader(s));
                String line = null;
                while (true) {
                    if ((line = blockingReadLine(reader)) != null) {
                        VibrationVTQ vvtq = parseVibrationStream(line);
                        if (vvtq != null) {
                            buffer.add(vvtq);
                        }
                    } else {
                        if (p != null) p.destroyForcibly();
                        //logger.error("Error pinging: ");
                        break;
                    }
                }
            } catch (Exception e) {
                if (p != null) p.destroyForcibly();
                logger.error("Error pinging: " + e.getLocalizedMessage());
                e.printStackTrace();
            }
            logger.info("Vibration stream completed SUCCESSFULLY SUCCESSFULLY");
        }

    }

    private String blockingReadLine(BufferedReader br) throws Exception {
        //this function returns a String only if the end of the line has been reached, not for EOF
        char s;
        String output = "";
        int read;
        int count = 0;
        try {
            while ((read = br.read()) != 10) {
                if (read != -1) {
                    output = output + (char) read;
                } else {
                    count = count + 1;
                    Thread.sleep(0, 50);
                }

                if (count > 1000) {
                    //logger.error("The blockingReadLine function has read more than 1000 characters. Aborting...: " + output);
                    return null;
                }
            }
            return output;
        } catch (Exception e) {
            logger.info("Error in blockingReadLine: " + e.getLocalizedMessage());
            e.printStackTrace();
            return null;
        }
    }

    private VibrationVTQ parseVibrationStream(String output) {
        /**
         * Sample input:
         * 2017-02-07 00:24:36.896764,-3683,30,1495
         */
        VibrationVTQ vibrationVTQ = new VibrationVTQ();
        String parsed[] = output.split(",");
        try {
            Instant myInstant = Instant.ofEpochMilli(
                    Math.round(Double.parseDouble(parsed[0])));
            vibrationVTQ.timestamp = myInstant.toEpochMilli() + Math.round(myInstant.getNano()/1000.0)/1000.0;
        } catch (Exception e) {
            //logger.error("Error parsing the timestamp. Output is: " + output);
            vibrationVTQ.timestamp = new Date().getTime();
        }
        try {

            vibrationVTQ.vibration_x = Double.valueOf(parsed[1]) * 4 / 16384.0;
            vibrationVTQ.vibration_y = Double.valueOf(parsed[2]) * 4 / 16384.0;
            vibrationVTQ.vibration_z = Double.valueOf(parsed[3]) * 4 / 16384.0;
            vibrationVTQ.magnitude = Math.sqrt(
                    Math.pow(vibrationVTQ.vibration_x, 2) +
                            Math.pow(vibrationVTQ.vibration_y, 2) +
                            Math.pow(vibrationVTQ.vibration_z, 2)) - 1;
        } catch (Exception e) {
            logger.error("The value this choked on was: " + parsed);
        }

        return vibrationVTQ;
    }
}