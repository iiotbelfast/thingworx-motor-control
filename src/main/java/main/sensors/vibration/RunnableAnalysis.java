package main.sensors.vibration;


/**
 * Created by kpashov on 13/02/2017.
 */


import com.thingworx.metadata.DataShapeDefinition;
import com.thingworx.metadata.FieldDefinition;
import com.thingworx.types.BaseTypes;
import com.thingworx.types.InfoTable;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.primitives.NumberPrimitive;
import com.thingworx.types.primitives.StringPrimitive;
import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.StatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Optional;


public class RunnableAnalysis implements Runnable {
    private LinkedList<VibrationVTQ> buffer;
    private static final Logger logger = LoggerFactory.getLogger(RunnableAnalysis.class);
    private int fft_size;
    private int low_res_fft;
    private FftFrame fft_buffer[];
    private int buffer_counter;
    private double TIMESTEP;
    private double magnitudeRMS;
    private boolean learning = true;
    private InfoTable fftInfoTable;
    private int learning_lookback = 100;
    private double anomaly_level = 0.0;

    private double[] freq_stdev_global;
    private double[] freq_avg_global;


    public RunnableAnalysis(LinkedList<VibrationVTQ> buffer, int fft_size, int low_res_fft, double timestep) {
        //pass datapoint buffer
        //pass fft_size
        //pass low_res_fft window
        //pass timestep

        this.buffer = buffer;
        this.fft_size = fft_size;
        this.low_res_fft = low_res_fft;
        this.TIMESTEP = timestep;

        this.freq_stdev_global = new double[low_res_fft];
        this.freq_avg_global = new double[low_res_fft];

        //logger.info("Create new vibration measurement thread successfully.");
    }

    public void run() {
        while (true){
            try{
                //logger.info("Buffer size: " + buffer.size());
                if (buffer.size() > fft_size * (TIMESTEP + 1) + 1){
                    this.magnitudeRMS = getVibrationMagnitudeRMS();
                    getVibrationFFT();
                    getAnomaly(learning_lookback);
                    this.fftInfoTable = getFFT_InfoTable(low_res_fft);
                    bufferCleanup();
                    //logger.info("Vibration analysis thread run successfully. Vibration RMS: " + magnitudeRMS);
                } else {
                    //logger.info("Not enough data data to begin analysis, sleeping for a bit...");
                    Thread.sleep(1000);
                }
            } catch (Exception e){
                //logger.error("Could not run vibration analysis thread");
            }
        }
    }

    public Double getVibrationRMS(){
        return magnitudeRMS;
    }

    public InfoTable getFFTInfotable(){
        InfoTable it = fftInfoTable;
        return it;
    }

    private Double getVibrationMagnitudeRMS() {
        int last = buffer.size() - 1;

        if (fft_size * 2 > last + 1) {
            //logger.warn("Not enough data to perform RMS computation. Aborting...");
            return null;
        }


        double myArray[] = new double[fft_size];


        for (int i = last; i >= 0; i--) {
            try {
                if (last - i < fft_size) {
                    double magnitude;
                    magnitude = buffer.get(i).magnitude;
                    myArray[last - i] = magnitude;
                }
            } catch (Exception e) {
                //logger.error("Error populating array for calculation. " +
                //        "Index: " + i +
                //        " last: " + last +
                //        " buffer size: " + buffer.size());
                //logger.error("The value was: " + buffer.get(i).timestamp + "," + buffer.get(i).magnitude);
                e.printStackTrace();
            }
        }
        return Math.sqrt(StatUtils.sumSq(myArray) / fft_size);
    }


    private void bufferCleanup() {
        for (int i = buffer.size() - 1;i >= 0; i--){
            try{
                buffer.get(i);
            }
            catch (NullPointerException e){
                //logger.error("Index " + i + " is null");
            }
        }

        while (buffer.size() - (fft_size * (TIMESTEP + 2) + 1) > 0) {
            buffer.removeFirst();
        }
        //logger.info("Cleanup performed succesffully.");
    }

    private FftFrame getVibrationFFT() {

        if (buffer.size() < fft_size) return null;
        if (buffer.getLast().timestamp - buffer.getFirst().timestamp <= fft_size * TIMESTEP) return null;

        int last = buffer.size() - 1;

        //logger.info("Calculating fft on a buffer of size: " + buffer.size());

        //in order to easily work with the buffer data, we need to put it in an array structure
        //FFT needs an array twice as big to store its results
        double[] myarray = new double[fft_size * 2];
        try {
            getResampledTimeseries(last, myarray, TIMESTEP);
        } catch (Exception e) {
            //logger.error("There was a problem getting a resampled timeseries. Aborting...");
            e.printStackTrace();
            return null;
        }

        /**
         * Calculate the real forward full Fast Fourier Transform on the timeseries
         */

        DoubleFFT_1D doubleFFT_1D = new DoubleFFT_1D(fft_size);
        doubleFFT_1D.realForwardFull(myarray);
        FftFrame fftFrame;
        try {
            fftFrame = new FftFrame(fft_size);
        } catch (Exception e) {
            //logger.error("Error creating a new FftFrame. Aborting...");
            e.printStackTrace();
            return null;
        }

        for (int i = 0; i < fft_size; i++) {
            fftFrame.magnitude[i] = Math.sqrt(Math.pow(myarray[i * 2], 2) + Math.pow(myarray[i * 2 + 1], 2));
            fftFrame.frequency[i] = Math.round(1 / TIMESTEP * i);
        }

        setLearningFrame(fftFrame);

        return fftFrame;
    }

    private ResampledTimeseries getResampledTimeseries(int startindex, double fft_array[], double timestep) throws Exception {
        /**
         * This array will contain the values that will be Fourier-Transformed for output
         */

        int original_size = fft_array.length / 2; //size of the array to fit the data into

        if (startindex <= 0 || startindex > buffer.size() - 1) {
            //logger.error("Trying to access values that do not exist in the buffer");
            throw new Exception();
        } else if (buffer.get(startindex).timestamp - buffer.get(0).timestamp < timestep * original_size) {
            //logger.error("Not enough data for calculation... ");
            throw new Exception();
        }

        Integer curindex = startindex; //the initial datapoint in the time-bin
        Integer endindex; //the final datapoint in the time-bin

        ResampledTimeseries resampledTimeseries = new ResampledTimeseries(original_size);

        double start_timestep = buffer.get(curindex).timestamp;
        //this loop fills in the array for the fft
        //there is a strong possibility of there being periods without datapoints
        //we will fix that when (if) it comes
        java.util.List<Integer> nulls = new ArrayList<Integer>();

        for (int i = 0; i < original_size; i++) {
            //get the range of indices between the two timesteps

            Integer index;

            //if the next datapoint does not fit...
            if (buffer.get(curindex).timestamp > (start_timestep  + timestep*i)) {
                index = null;
            } else {
                index = this.getTimestepIndices(curindex, TIMESTEP);
                resampledTimeseries.timestamp[i] = start_timestep + timestep / 2 + i * timestep;
            }

            if (index != null) {
                //happy path, there were enough datapoints to do the estimation.
                //go back in time and average
                endindex = index;
                try {
                    for (int j = curindex; j >= endindex; j--) {
                        resampledTimeseries.magnitude[i] = +buffer.get(j).magnitude / (curindex - endindex + 1);
                    }
                } catch (Exception e) {
                    //logger.error("Error getting some data: " +
                    //        "curindex: " + curindex +
                    //        ", endindex: " + endindex +
                    //        ", buffer size: " + buffer.size() +
                    //        ", i: " + i);
                }
                //fill in the fft array
                fft_array[i] = resampledTimeseries.magnitude[i];
                curindex = endindex - 1;
            } else {
                //logger.info("There are no values between " + buffer.get(curindex).timestamp + " and " + (buffer.get(curindex).timestamp + timestep));
                nulls.add(i);
            }
        }

        //fix the empty spaces by interpolating
        NumberFormat mf = new DecimalFormat("0.####");
        NumberFormat ts = new DecimalFormat("0.######");
        Optional<Integer> min = nulls.stream().min(Comparator.<Integer>naturalOrder());
        Optional<Integer> max = nulls.stream().max(Comparator.<Integer>naturalOrder());

        //if it has found both a min and a max...
        if (min.isPresent() && max.isPresent()) {

            for (Integer I : nulls) {
                int start = I - 1;
                int end;

                for (int i = start; i < original_size; i++) {
                    try {
                        if (ArrayUtils.contains(nulls.toArray(), i)) {
                            end = i;
                            resampledTimeseries.magnitude[I] =
                                    ((resampledTimeseries.magnitude[start] +
                                            resampledTimeseries.magnitude[end]) / (start - end)) * I +
                                            resampledTimeseries.magnitude[I - 1];
                            fft_array[I] = resampledTimeseries.magnitude[I];
                            //logger.warn("Filling in data for timestamp: "
                            //        + ts.format(resampledTimeseries.timestamp[I]) +
                            //        " magnitude: " + mf.format(resampledTimeseries.magnitude[I]));
                            break;
                        }
                    } catch (Exception e) {
                        //logger.error("There is no array element with such index. I: " + I + " i: " + i);
                        e.printStackTrace();
                    }
                }
            }
        }
        return resampledTimeseries;
    }

    /**
     * Returns the index of the last value of the buffer where the timestamp
     * is smaller than required
     *
     * @param startindex
     * @return
     */
    private Integer getTimestepIndices(int startindex, double timestep) {
        Integer output = new Integer(0);
        output = null;
        double initial_timestamp;
        try {
            initial_timestamp = buffer.get(startindex).timestamp;
        } catch (NullPointerException e) {
            //logger.error("Error retrieveing value from buffer. " +
            //        "startindex: " + startindex +
            //        ", buffer size: " + buffer.size());
            //logger.error("Value is: " + buffer.get(startindex).toString());
            return null;
        } catch (NegativeArraySizeException e) {
            //logger.error("No more values" +
            //        "startindex: " + startindex +
            //        ", buffer size: " + buffer.size());
            return null;
        }
        int i = startindex;

        try {
            do {
                //go backwards in time, looking for measurements within the required timestamp
                if (buffer.get(i).timestamp > (initial_timestamp - timestep) && (i >= 0)) {
                    i--;
                } else {
                    //go a step forward in time because we have overdone it...
                    //this can potentially output the same as startindex...
                    output = i + 1;
                }
            } while (output == null);
            return output;
        } catch (NullPointerException e) {
            //logger.warn("The timestep is too small and there are no measurements "
            //        + timestep + " milliseconds after the last measurement: " + i);
            e.printStackTrace();
            return null;
        } catch (IndexOutOfBoundsException e) {
            //logger.error("No more measurements in the buffer.");
            e.printStackTrace();
            return null;
        } catch (Exception e){
            //logger.error("Thread sleep interrupded.");
            e.printStackTrace();
            return null;
        }
    }

    private InfoTable getFFT_InfoTable(int bins) {

        FftFrame low_resFFT = fft_buffer[buffer_counter - 1];

        if (low_resFFT != null) {


            DataShapeDefinition dsd = new DataShapeDefinition();
            dsd.addFieldDefinition(new FieldDefinition("Frequency", BaseTypes.STRING));
            dsd.addFieldDefinition(new FieldDefinition("Magnitude", BaseTypes.NUMBER));
            InfoTable result = new InfoTable(dsd);

            // Define the fields

            String text = "Magnitude and frequency: ";
            for (int fooIndex = 0; fooIndex < bins; fooIndex++) {
                try {
                    ValueCollection vc = new ValueCollection();

                    vc.SetStringValue("Frequency", new StringPrimitive(String.valueOf(low_resFFT.frequency[fooIndex])));
                    vc.SetNumberValue("Magnitude", new NumberPrimitive(low_resFFT.magnitude[fooIndex]));
                    result.addRow(vc);

//                    text = text + ", " + fooIndex +
//                            " [" +  fftFrame.frequency[fooIndex] +
//                            "," + fftFrame.magnitude[fooIndex] + "]";

                } catch (Exception e) {
                    //logger.error("Error parsing to JSON: " + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
//            logger.info(text);
            //logger.info("Infotable returned.");
            return result;
        }
        //logger.error("No infotable to return, buffer_counter at " + buffer_counter);
        return null;
    }

    private FftFrame reduceFFTresolution(int bins, FftFrame fftFrame) {
        FftFrame outputFFT;
        try {
            outputFFT = new FftFrame(bins);
        } catch (Exception e) {
            //logger.error("Cannot create output FftFrame");
            e.printStackTrace();
            return null;
        }


        for (int binIndex = 0; binIndex < bins; binIndex++) {

            Double bin_size = ((double) fftFrame.length() / 2) / bins; //how many high-precision bins fit in a low-precision one
            Double partial_high = 1.0 - ((((double) binIndex) * bin_size) % 1); //how much of the bottom bin needs to be grabbed
            Double partial_low = (((double) binIndex + 1) * bin_size) % 1; //how much of the top bin needs to be grabbed
            //logger.debug("reducing Resolution. Bin_size: " + bin_size +
            //        " partial_high " + partial_high +
            //        " partial_low " + partial_low);
            //logger.debug("bin index start: " + (binIndex * bin_size));
            //logger.debug("bin index end: " + ((binIndex + 1) * bin_size));
            //logger.debug("binIndex: " + binIndex);
            int startIndex = (int) Math.floor(binIndex * bin_size);

            //handle the case where the beginning of the bins does not align
            if ((binIndex * bin_size) % 1 != 0) {
                //the first bin does not start exactly where the high-resolution bins start
                outputFFT.magnitude[binIndex] += (fftFrame.magnitude[(int) (bin_size * binIndex)] * partial_high) / bin_size;
                outputFFT.frequency[binIndex] += (fftFrame.frequency[(int) (bin_size * binIndex)] * partial_high) / bin_size;
            }

            if (Math.floor((binIndex + 1) * (bin_size)) - Math.ceil((binIndex) * bin_size) > 0) {
                //this if might be redundant...
                //handle the middle if it falls entirely within the required stack frame
                for (int i = (int) Math.ceil(binIndex * bin_size); i < (int) Math.floor((binIndex + 1) * bin_size); i++) {
                    //add a bit of the bins to each
                    outputFFT.magnitude[binIndex] += fftFrame.magnitude[i] / bin_size;
                    outputFFT.frequency[binIndex] += fftFrame.frequency[i] / bin_size;
                }
            }

            //handle the top
            if ((binIndex * bin_size) % 1 != 0) {
                outputFFT.magnitude[binIndex] += (fftFrame.magnitude[(int) Math.ceil(binIndex * bin_size) - 1] * partial_low) / bin_size;
                outputFFT.frequency[binIndex] += (fftFrame.frequency[(int) Math.ceil(binIndex * bin_size) - 1] * partial_low) / bin_size;
            }

            //logger.debug("bin magnitude: " + outputFFT.magnitude[binIndex]);
            //logger.debug("bin frequency: " + outputFFT.magnitude[binIndex]);
        }

        return outputFFT;
    }

    public Anomaly getAnomaly(){
        Anomaly anomaly = new Anomaly();

        anomaly.certainty = anomaly_level;

        if (learning == true) {
            anomaly.status = AnomalyStatus.LEARNING;
        } else{
            if (anomaly_level > 0.9 && learning == false){
                anomaly.status = AnomalyStatus.ANOMALY;
            } else if (anomaly_level < 0.9 && learning == false) {
                anomaly.status = AnomalyStatus.NORMAL;
            }
        }


        return anomaly;
    }

    private Anomaly getAnomaly(int lookback) {
        DecimalFormat df = new DecimalFormat("0.############");
        double[] average = new double[fft_buffer[0].frequency.length];
        double[] stDev = new double[fft_buffer[0].frequency.length];
        //logger.info("There are " + buffer_counter + " fft frames");


        if ((this.buffer_counter >= lookback) && (learning == true)) {
            //logger.info("Calculating average and standard deviation...");
            learning = false;
            //iterate over all the
            try {
                PrintWriter writer = new PrintWriter("VibrationConfig", "UTF-8");
                writer.println("Magnitude, stDev");
                for (int frequency_bin = 0; frequency_bin < fft_buffer[0].frequency.length; frequency_bin++) {
                    double fft_magnitude_buf[] = new double[buffer_counter];
                    String text ="";
                    //iterate over all the frames, i.e. average over time
                    for (int fft_frame = 0; fft_frame < buffer_counter - 1; fft_frame++) {
                        fft_magnitude_buf[fft_frame] = fft_buffer[fft_frame].magnitude[frequency_bin];
                        text = text +","+ fft_buffer[fft_frame].magnitude[frequency_bin];
                    }

                    average[frequency_bin] = StatUtils.mean(fft_magnitude_buf,0,buffer_counter);
                    stDev[frequency_bin] = Math.sqrt(StatUtils.populationVariance(fft_magnitude_buf,0,buffer_counter));
                    writer.println(df.format(average[frequency_bin]) + "," + df.format(stDev[frequency_bin]));
                    //logger.info(df.format(average[frequency_bin]) + "," + df.format(stDev[frequency_bin])+":" + text);
                }
                writer.close();
            } catch (IOException e) {
                //logger.error("Error calculating average and standard deviation for bins");
                e.printStackTrace();
                // do something
            }


            freq_avg_global = average.clone();
            freq_stdev_global = stDev.clone();


            //logger.info("Learned...");

        } else if ((learning == false) && (this.buffer_counter >= lookback)) {
            //evaluate current frequency spectrum against the known one...
            //iterate over each frequency bin and check how "outliery" it is...
            int outlier_count = 0;
            for (int frequency_bin = 0; frequency_bin < fft_buffer[0].frequency.length; frequency_bin++) {
                double stDevdbl = freq_stdev_global[frequency_bin];
                double avgDbl = freq_avg_global[frequency_bin];

                if (fft_buffer[buffer_counter - 1].magnitude[frequency_bin] > freq_avg_global[frequency_bin] + 3 * freq_stdev_global[frequency_bin]){
                    //logger.warn("WARNING m:" + fft_buffer[buffer_counter - 1].magnitude[frequency_bin] +
                    //        " f:" + fft_buffer[buffer_counter - 1].frequency[frequency_bin] + " outside of range: " +
                    //        (freq_avg_global[frequency_bin] - 3 * freq_stdev_global[frequency_bin]) + " to " +
                    //        (freq_avg_global[frequency_bin] +3 * freq_stdev_global[frequency_bin]));
                    outlier_count++;
                } else if(fft_buffer[buffer_counter - 1].magnitude[frequency_bin] < freq_avg_global[frequency_bin] - 3 * freq_stdev_global[frequency_bin]){
                    //logger.warn("WARNING m:" + fft_buffer[buffer_counter - 1].magnitude[frequency_bin] +
                    //        " f:" + fft_buffer[buffer_counter - 1].frequency[frequency_bin] + " outside of range: " +
                    //        (freq_avg_global[frequency_bin] - 3 * freq_stdev_global[frequency_bin]) + " to " +
                    //        (freq_avg_global[frequency_bin] +3 * freq_stdev_global[frequency_bin]));
                    outlier_count++;
                } else {

                }
            }

            if (outlier_count > 3){
                //logger.warn("WARNING there are " + outlier_count + " outliers in the frequency spectrum.");
                anomaly_level = anomaly_level + (1-anomaly_level) * 0.1;
            } else {
                anomaly_level = anomaly_level * 0.9;
            }
            //logger.info("Anomaly Level: " + anomaly_level);
        } else {
            //logger.info("Deloitte Spectral Learning in progress...");
        }
        return null;
    }

    public void resetLearning(){
        fft_buffer=null;
        buffer_counter=0;
        learning=true;
        anomaly_level = 0;
    }

    private void setLearningFrame(FftFrame fftFrame){
        if (fft_buffer == null) {
            fft_buffer = new FftFrame[fft_size];
            fft_buffer[0] = reduceFFTresolution(low_res_fft, fftFrame);
            buffer_counter = 1;
        } else {
            if (buffer_counter == fft_buffer.length) {
                buffer_counter = 1;
            } else {
                buffer_counter = buffer_counter + 1;
            }
            fft_buffer[buffer_counter - 1] = reduceFFTresolution(low_res_fft, fftFrame);
        }
    }
}