package main.sensors.vibration;


/**
 * Created by kpashov on 07/02/2017.
 */


public class VibrationVTQ {

    // Property Declarations
    public double vibration_x;
    public double vibration_y;
    public double vibration_z;
    public double magnitude;
    public double timestamp;


    public VibrationVTQ(double vibration_x, double vibration_y, double vibration_z, double magnitude, double timestamp){
        this.vibration_x = vibration_x;
        this.vibration_y = vibration_y;
        this.vibration_z = vibration_z;
        this.timestamp = timestamp;
        this.magnitude = magnitude;
    }


    public VibrationVTQ(){
        super();
    }


    @Override
    public String toString(){
        String text;

        text = "v_x:" + vibration_x + ",v_y:" + vibration_y + ",v_z" + vibration_z + ",m" + magnitude + ",t" + timestamp;
        return text;
    }

}
