package main.sensors.vibration;


/**
 * Created by kpashov on 09/02/2017.
 */


public class FftFrame {
    public double magnitude_x[];
    public double magnitude_y[];
    public double magnitude_z[];
    public double magnitude[];
    public double frequency[];
    public double rms;
    private Integer length = null;

    public FftFrame(int fft_size) throws Exception{
        if (length != null){
            throw new Exception();
        }
        length = fft_size;
        this._fftFrame(length);
    }

    public FftFrame(int fft_size, boolean directions) throws Exception{
        if (length != null){
            throw new Exception();
        }
        this._fftFrame(fft_size);
        this._directions(fft_size);

    }

    private void _fftFrame(int fft_size){
        magnitude = new double[fft_size];
        frequency = new double[fft_size];
    }

    private void _directions(int fft_size){
        magnitude_y = new double[fft_size];
        magnitude_z = new double[fft_size];
        magnitude_x = new double[fft_size];
    }

    public int length(){
        return length;
    }
}