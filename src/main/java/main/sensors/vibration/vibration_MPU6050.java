package main.sensors.vibration;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 *
 *  Description:    This is the main vibration class
 */


// Import Declarations
import com.thingworx.types.InfoTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.LinkedList;


public class vibration_MPU6050 {

    // Property Declarations
    private Thread vibt;
    private Thread analysist;
    private MPU6050Vibration mpu6050vibration;
    private RunnableAnalysis mpu6050RunnableAnalysis;
    private LinkedList<VibrationVTQ> buffer = new LinkedList<VibrationVTQ>();
    private static final Logger logger = LoggerFactory.getLogger(vibration_MPU6050.class);
    private int fft_size = 1024;
    private int low_res_fft = 16;
    private double TIMESTEP = 3.0;
    private String id = "";
    private String description = "";
    private float value = 0.0f;


// =====================================================================================================================


    // Object Constructor
    public vibration_MPU6050(String id, String description) {
        this.id = id;
        this.description = description;
        this.setValue((float)0.0);

        mpu6050vibration = new MPU6050Vibration(buffer);
        vibt = new Thread(mpu6050vibration);
        vibt.start();

        mpu6050RunnableAnalysis = new RunnableAnalysis(buffer, fft_size, low_res_fft, TIMESTEP);
        analysist = new Thread(mpu6050RunnableAnalysis);
        analysist.start();
    }


// =====================================================================================================================


    private void setID (String id) {
        this.id = id;
    }

    private void setDescription (String id) {
        this.id = id;
    }

    private void setValue (float sensorValue) {
        this.value = sensorValue;
    }


    // --------------------------------------------------------------------


    public synchronized float getValue () {
        this.value = (float) getVibrationMagnitudeRMS();
        return this.value;
    }


    private String getDescription () {
        return this.description;
    }

    public String getID () {
        return this.id;
    }

    public double getVibrationMagnitudeRMS(){
        return mpu6050RunnableAnalysis.getVibrationRMS();
    }

    public InfoTable getFFT_InfoTable(){
        logger.info("Infotable requested.");
        return mpu6050RunnableAnalysis.getFFTInfotable();
    }

    public Anomaly getAnomaly(){
        return mpu6050RunnableAnalysis.getAnomaly();
    }

    public String getStatus(){
        Anomaly anomaly = mpu6050RunnableAnalysis.getAnomaly();
        switch (anomaly.status){
            case ANOMALY:
                return "ANOMALY";
            case LEARNING:
                return  "LEARNING";
            case NORMAL:
                return "MONITORING";
            default:
                return "LEARNING";

        }
    }

    public boolean getAnomalyBool(){
        Anomaly anomaly = mpu6050RunnableAnalysis.getAnomaly();
        if (anomaly.certainty > 0.95){
            return true;
        } else {
            return  false;
        }
    }

    public void resetLearning(){
        mpu6050RunnableAnalysis.resetLearning();
    }

}
