package main.sensors.vibration;


/**
 * Created by kpashov on 14/02/2017.
 */


public enum AnomalyStatus {
    LEARNING, NORMAL, ANOMALY
}