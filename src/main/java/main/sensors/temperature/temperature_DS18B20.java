package main.sensors.temperature;


/**
 * Created by:      Fred Burder      fburder@deloitte.co.uk
 * and              Steven Bailie    sbailie@deloitte.co.uk
 * on 22/AUG/2017
 */


// Import Declarations
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class temperature_DS18B20 {

    // This directory created by 1-wire kernel modules
    static String sensorRootPath = "/sys/bus/w1/devices";


    // Property Declarations
    private String id = "";
    private String description = "";
    private float value = 0.0f;


// =====================================================================================================================


    // Object Constructor
    public temperature_DS18B20(String id, String description) {
        this.id = id;
        this.description = description;
        this.setValue(0.0f);
    }


// =====================================================================================================================


    private void setID (String id) {
        this.id = id;
    }

    private void setDescription (String description) {
        this.description = description;
    }

    private void setValue (float sensorValue) {
        this.value = sensorValue;
    }


    // --------------------------------------------------------------------


    public float getValue () {
        // Read Temperature from Sensor stream file
        // ----------------------------------------
        File dir = new File(sensorRootPath);
        // Device data in w1_slave file
        String filePath = sensorRootPath + "/" + this.id + "/w1_slave";
        File f = new File(filePath);
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            String output;
            while ((output = br.readLine()) != null) {
                int idx = output.indexOf("t=");
                if (idx > -1) {
                    // Temp data (multiplied by 1000) in 5 chars after t=
                    float tempC = Float.parseFloat(output.substring(output.indexOf("t=") + 2));
                    // Divide by 1000 to get degrees Celsius
                    tempC /= 1000;
                    return tempC;
                }
            }
        } catch (Exception ex) {
            System.out.println("ERROR: Unable to retrieve sensor data stream");
            System.out.println(ex.getMessage());
        }
        return 0.0f;
    }


    private String getDescription () {
        return this.description;
    }

    public String getID () {
        return this.id;
    }

}