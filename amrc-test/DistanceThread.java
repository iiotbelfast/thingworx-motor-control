import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

/**
 * Created by kpashov on 19/01/2017.
 */
public class DistanceThread implements Runnable {

    public static final int ARDUINO_ADDR = 0x04;
    private I2CBus i2c;
    I2CDevice arduino;
    double averageNanosRead;

    private List<DistanceVT> buffer;
    private float[] capsules = new float[2];

    private String[] args = new String[]{"python", "./run-scripts/distance.py"};


    public DistanceThread(LinkedList<DistanceVT> list, float[] capsules) {
        buffer = list;
        this.capsules = capsules;
        byte results[] = new byte[33];
        try{
            i2c = I2CFactory.getInstance(I2CBus.BUS_1);
            arduino = i2c.getDevice(ARDUINO_ADDR);
            arduino.read(results,0,33);
            //log.info("Response: " + results);
        } catch (Exception e){
            //log.error("Error opening a new i2c bus.");
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        byte results[] = new byte[32];
        byte old_results[] = new byte[32];
        long timeStart = System.nanoTime();
        long estimaatedTime=0;
        while (true) {
            try{
                arduino.read(results,0,32);
                if (!Arrays.equals(results,old_results)){
                    estimaatedTime = System.nanoTime() - timeStart;
                    List<DistanceVT> newVals = getDistancesFromByteArray(results);
                    old_results = results.clone();
                    for (DistanceVT vt : newVals){
                        buffer.add(vt);
                    }

                    if (estimaatedTime != 0){
                        averageNanosRead = averageNanosRead*0.99 + 0.01*estimaatedTime;
                    }
                    timeStart = System.nanoTime();
                    estimaatedTime =0 ;
                }
                Thread.sleep(50);
            } catch (Exception e){
                //log.error("Error getting data from Arduino.");
                e.printStackTrace();
            }
        }
    }

    private List<DistanceVT> getDistancesFromByteArray(byte[] response){
        List<DistanceVT> myList=new LinkedList<>();
        for(int i=0; i<8; i++){
            DistanceVT myVT;
            myVT = getDistanceFromByteArray(Arrays.copyOfRange(response,i*4,(i+1)*4));
            if (myVT!= null){
                myList.add(myVT);
            }
        }
        return  myList;
    }

    private DistanceVT getDistanceFromByteArray(byte[] byte_values, int feeder){
        if (byte_values == null ||
                byte_values.length !=4){
            //log.error("Wrong argument passed to byte_values, returning null.");
            return null;
        }
        DistanceVT myDist = new DistanceVT();
        byte arr[] = Arrays.copyOfRange(byte_values, 0,4);
        double value = (double) ByteBuffer.wrap(arr).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        if (value!=Double.NaN){
            myDist.value = (float) ByteBuffer.wrap(arr).order(ByteOrder.LITTLE_ENDIAN).getFloat();
            return myDist;
        } else {
            return null;
        }

    }
}