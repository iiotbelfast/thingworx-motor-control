import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.util.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

public class MainTest {

   	//Global User Inputs//////////////////////////////////////////////////////////////////////////////////////
   	public static int durationRMS = 3;					//loop length per RMS polling in seconds.
   	public static int calibrationTime = 10;				//time spent calculating a zero average for calibration in seconds.
	public static int toggleCycle = 6000;					//number of display cycles before the relay toggles on or off.
	public static int cyclesUntilEnd = 10000;				//number of display cycles until program ends.
	public static int currentChannel = 1;				//visual number of the ADC port which the current sensor wire is connected to.
	public static int voltageChannel = 2;				//visual number of the ADC port which the voltage sensor wire is connected to.
	public static double currentmVperAmp = 185*(2/2.5); //depends on ACS712 version. (Use 185 for 5A, use 100 for 20A Module and 66 for 30A Module). Also the sensor was consistently reading 2Amps when it should have been 2.5Amp, so we added the multiplier
	public static double voltagemVperVolt = 1.2;		//This value was found by using a multimeter and calculating what the value should be.
	public static double currentNoiseLimit = 0.23;	 	//This counteracts the small noise around 0.0 that creates a false RMS reading. Anything under this will be ignored.		
	public static double voltageNoiseLimit = 10.0;		//This counteracts the small noise around 0.0 that creates a false RMS reading. Anything under this will be ignored.
    public static final GpioController gpio = GpioFactory.getInstance(); // Create gpio controller and pins
	public static final GpioPinDigitalOutput relayPin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "relay", PinState.LOW);
	public static final int ARDUINO_ADDR = 0x04;
    private I2CBus i2c;
    I2CDevice arduino;
	double averageNanosRead;
	public static float tempAmbient;
	public static float tempInternal;
	private List<DistanceVT> buffer;
	private DistanceThread distanceThread;
    private Thread t;
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	public void main(String[] args) throws InterruptedException {
	
		//Initialise Values///////////////////////////////////////////////////////////////////////////////////////
		float current = 0;
		float voltage = 0;
		float vibration = 0;
        relayPin.setShutdownOptions(true, PinState.LOW);
		System.out.println("----------------------------------------------------------------------------------------------");
		System.out.println("--> MainTest.java Started");
		System.out.println("----------------------------------------------------------------------------------------------");
		//End of Initialise Values///////////////////////////////////////////////////////////////////////////////
		
        distanceThread = new DistanceThread(buffer, capsules);
        t = new Thread(distanceThread);
        t.start();
	
		//Begin of Display Loop////////////////////////////////////////////////////////////////////////////////
		relayPin.low();
        System.out.println("--> Relay OFF");
        Thread.sleep(5000);
		int count = 1;
		relayPin.high();
		System.out.println("--> Relay ON");
		System.out.println("----------------------------------------------------------------------------------------------");
		while (count < cyclesUntilEnd) {
			readArduino();
			Temperature.readTemp();
			current = 0;
			voltage = 0;
			vibration = 0;
			//current = Math.round(current * 100.0) / 100.0;
			//voltage = Math.round(voltage * 10.0) / 10.0;
			System.out.printf("%s %5.2f %s %5.2f %s %3.2f %s %4.1f %n", "Ambient Temp = ", tempAmbient, "|| Internal Temp = ", tempInternal, "|| Current: ", current, "|| Voltage: ", voltage);
			if ((count % toggleCycle) == 0)
			{
				relayPin.toggle();
				System.out.println("----------------------------------------------------------------------------------------------");
				System.out.println("--> Relay toggled every " + toggleCycle + " cycles");
				System.out.println("----------------------------------------------------------------------------------------------");
			}
			count ++;
		}
        gpio.shutdown();
        System.out.println("--> Exiting MainTest.java");
		System.out.println("----------------------------------------------------------------------------------------------");
		//End of Display Loop//////////////////////////////////////////////////////////////////////////////////
    }
	
    public void readArduino() {
        byte results[] = new byte[32];
        byte old_results[] = new byte[32];
        long timeStart = System.nanoTime();
        long estimaatedTime=0;
        while (true) {
            try{
                arduino.read(results,0,32);
                if (!Arrays.equals(results,old_results)){
                    estimaatedTime = System.nanoTime() - timeStart;
                    List<DistanceVT> newVals = getDistancesFromByteArray(results);
                    old_results = results.clone();

                    for (DistanceVT vt : newVals){
                        buffer.add(vt);
                    }

                    if (estimaatedTime != 0){
                        averageNanosRead = averageNanosRead*0.99 + 0.01*estimaatedTime;
                    }

                    timeStart = System.nanoTime();
                    estimaatedTime =0 ;
                }
                Thread.sleep(50);
            } catch (Exception e){
                //log.error("Error getting data from Arduino.");
                e.printStackTrace();
            }
        }
    }


	
	private List<DistanceVT> getDistancesFromByteArray(byte[] response){
    List<DistanceVT> myList=new LinkedList<>();
		for(int i=0; i<8; i++){
			DistanceVT myVT;
			myVT = getDistanceFromByteArray(Arrays.copyOfRange(response,i*4,(i+1)*4));
			if (myVT!= null){
				myList.add(myVT);
			}
		}
		return  myList;
	}

    private DistanceVT getDistanceFromByteArray(byte[] byte_values){
        if (byte_values == null || byte_values.length !=4){
            //log.error("Wrong argument passed to byte_values, returning null.");
            return null;
        }
        DistanceVT myDist = new DistanceVT();
        byte arr[] = Arrays.copyOfRange(byte_values, 0,4);
        double value = (double) ByteBuffer.wrap(arr).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        if (value!=Double.NaN){
            myDist.value = (float) ByteBuffer.wrap(arr).order(ByteOrder.LITTLE_ENDIAN).getFloat();
            return myDist;
        } else {
            return null;
        }
    }
}