#!/usr/bin/python
try:
    import smbus
    import time
    import math
    import datetime as DT
    import MPU6050registers as regs
    import sys

    f = open(sys.argv[1], 'w')

    # Power management registers
    power_mgmt_1 = 0x6b
    power_mgmt_2 = 0x6c
    MPU6050_RA_CONFIG = 0x1A
    MPU6050_SCALING=0x10

    epoch = DT.datetime.utcfromtimestamp(0)

    def read_byte(adr):
        return bus.read_byte_data(address, adr)

    def read_word(adr):
        high = bus.read_byte_data(address, adr)
        low = bus.read_byte_data(address, adr+1)
        val = (high << 8) + low
        return val

    def read_word_2c(adr):
        val = read_word(adr)
        if (val >= 0x8000):
            return -((65535 - val) + 1)
        else:
            return val

    def dist(a,b):
        return math.sqrt((a*a)+(b*b))

    def get_y_rotation(x,y,z):
        radians = math.atan2(x, dist(y,z))
        return -math.degrees(radians)

    def get_x_rotation(x,y,z):
        radians = math.atan2(y, dist(x,z))
        return math.degrees(radians)

    def unix_time_millis(dt):
        return (dt - epoch).total_seconds() * 1000.0

    bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
    address = 0x68       # This is the address value read via the i2cdetect command

    # Now wake the 6050 up as it starts in sleep mode
    bus.write_byte_data(address, power_mgmt_1, 0)
    bus.write_byte_data(address, regs.MPU6050_RA_SMPLRT_DIV, 0x07)
    bus.write_byte_data(address, regs.MPU6050_RA_CONFIG, 0)
    bus.write_byte_data(address, regs.MPU6050_RA_ACCEL_CONFIG, 0x10)

    while True:
        try:
            accel_xout_scaled = read_word_2c(0x3b)
            accel_yout_scaled = read_word_2c(0x3d)
            accel_zout_scaled = read_word_2c(0x3f)

            f.write( "%f,%i,%i,%i" % (round(unix_time_millis(DT.datetime.now()),4),accel_xout_scaled,accel_yout_scaled,accel_zout_scaled) + "\n");
            sys.stdout.flush()
        except Exception:
            time.sleep(100/1000000.0)
except Exception:
    import time
    import random
    import sys

    f = open(sys.argv[1], 'w')

    while True:
        try:
            f.write( "%f,%i,%i,%i" % (round(unix_time_millis(DT.datetime.now()),4),random.random()*10,random.random()*10,random.random()*10) * "\n")
            time.sleep(1/1000)
            sys.stdout.flush()
        except ImportError:
            time.sleep(100/1000000.0)






