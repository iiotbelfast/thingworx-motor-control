#!/usr/bin/env bash

pid=`ps aux | grep vibration.py | awk '{print $2}'`
sudo kill -9 $pid
touch /home/deloitte-admin/$1
nohup python /home/deloitte-admin/run-scripts/vibration.py >> /home/deloitte-admin/$1 2>&1 &
pid=`ps aux | grep vibration.py | awk '{print $2}'`
sudo renice -n -20 $pid